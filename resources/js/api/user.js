import axios from "../axios";
export function getUser(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/user/6147c2fbee15c4351e399e9d`,
        headers: {
            "Content-type": "application/json",
            "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNDdjMmZiZWUxNWM0MzUxZTM5OWU5ZCIsIm5hbWUiOiJTb25pIEZpcmRhdXMiLCJlbWFpbCI6InNvbmlmaXJkYXVzMzRAZ21haWwuY29tIiwibXNpc2RuIjoiMDk4Nzg0NCIsImlhdCI6MTY0MTE4NTkwNH0.lzIzix54NZWkb0QrnK0DAqCvSVoNu70ueL-V7hajlMs",
            // 'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}

export default {
    getUser,
};