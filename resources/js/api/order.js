import axios from "../axios";
export function getOrderList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/order?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}

export default {
    getOrderList,
};