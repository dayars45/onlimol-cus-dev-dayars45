import axios from "../axios";

export function getUlasanDetail(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/order/61cad15f72bc2c5254da9a10`,
        headers: {
            "Content-type": "application/json",
            'Authorization': "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYxNDdjMmZiZWUxNWM0MzUxZTM5OWU5ZCIsIm5hbWUiOiJTb25pIEZpcmRhdXMiLCJlbWFpbCI6InNvbmlmaXJkYXVzMzRAZ21haWwuY29tIiwibXNpc2RuIjoiMDk4Nzg0NCIsImlhdCI6MTY0MTE4NTkwNH0.lzIzix54NZWkb0QrnK0DAqCvSVoNu70ueL-V7hajlMs"
        }
    });
}

export default {
    getUlasanDetail
};