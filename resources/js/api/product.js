import axios from "../axios";
export function getProdukList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}
export function getProdukBaseSallerList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}
export function getOptionProductList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}
export function getTrendingProductList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}

export function getMoreProdukList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}
export function getReviewProdukList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/ulasan?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}
export function getDiscussProdukList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product_comment?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}
export function getOneProduct() {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product/61b9bc2ccd305e6ef9c6012d`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}

export default {
    getProdukList,
    getProdukBaseSallerList,
    getOptionProductList,
    getMoreProdukList,
    getTrendingProductList,
    getReviewProdukList,
    getDiscussProdukList,
    getOneProduct,
};