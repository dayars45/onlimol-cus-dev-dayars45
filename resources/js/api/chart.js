import axios from "../axios";

export function getHistoryProductsChart(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/product?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}

export default {
    getHistoryProductsChart
};