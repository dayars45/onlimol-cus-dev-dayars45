import axios from "../axios";
export function getCategoryList(props) {
    return axios({
        method: "GET",
        url: `${process.env.MIX_API_URL}/category?${props}`,
        headers: {
            "Content-type": "application/json",
            'x-api-key': `${process.env.MIX_API_KEY}`
        }
    });
}

export default {
    getCategoryList,
};