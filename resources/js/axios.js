import axios from 'axios';
axios.interceptors.request.use(
  (config) => {
    const newConfig = {
      ...config,
      ...{
        headers: {
          ...config.headers,
          'Content-type': 'application/json',
          // 'x-api-key': `${process.env.MIX_API_KEY}`
        }
      }
    };
    return newConfig;
  },
  err => Promise.reject(err),
);


export default axios;
