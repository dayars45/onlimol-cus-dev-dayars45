export function numberWithCommas(x) {
    if (isNaN(x) || !x) {
        return "0";
    }
    const parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}

export function numberWithCommasRp(x) {
  if (isNaN(x) || !x) {
      return "0";
  }
  const parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return `Rp ${parts.join(".")}`;
}

export function urlSlug(url){
    const link = url
    .replace(/\s+/g, "-") // space to -
    .replace(/&/g, `-and-`) // & to and
    .replace(/--/g, `-`); // -- to -
  return link;
}

export function kapital(str)
{  return str.replace (/\w\S*/g, 
      function(txt)
      {  return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); } );
}

export function discount(price, discount){
  return price - (price * discount) / 100
}


export function multiplePricing(variasi, sizes, price, discount){

  const pricingVariant = [];
  let stockVariant = 0;
  if(variasi.value.length > 0){
    for (let i = 0; i < variasi.value.length; i += 1) {
      for (let j = 0; j < variasi.value[i].sizes.length; j += 1) {
        stockVariant += variasi.value[i].sizes[j].stock;
        pricingVariant.push(variasi.value[i].sizes[j].price);
      }
    }

      const min = Math.min.apply(
      Math,
      pricingVariant
    );

    const max = Math.max.apply(
      Math,
      pricingVariant
    );
    if (min == max) {
      return numberWithCommas(min - (min * discount) / 100);
    }
    if (min != max) {
      return `${numberWithCommas(
        min - (min * discount) / 100
      )} - ${numberWithCommas(max - (max * discount) / 100)}`;
    }
  }

  if (sizes.filter((el) => el.price).length > 0) {

    const minSizes = Math.min.apply(
      Math,
      sizes.filter((el) => el.price).map((el) => el.price)
    );
    const maxSizes = Math.max.apply(
      Math,
      sizes.filter((el) => el.price).map((el) => el.price)
    );
    if (minSizes == maxSizes) {
      return numberWithCommas(minSizes - (minSizes * discount) / 100);
    }
    if (minSizes != maxSizes) {
      return `${numberWithCommas(
        minSizes - (minSizes * discount) / 100
      )} - ${numberWithCommas(maxSizes - (maxSizes * discount) / 100)}`;
    }
  }else{
      if(discount){
          return numberWithCommas(price - (price * discount) / 100);
      }
      return numberWithCommas(price);
  }
}

export function multiplePricingDiskon(variasi, sizes, price, discount){

  const pricingVariant = [];
  let stockVariant = 0;
  if(variasi.value.length > 0){
    for (let i = 0; i < variasi.value.length; i += 1) {
      for (let j = 0; j < variasi.value[i].sizes.length; j += 1) {
        stockVariant += variasi.value[i].sizes[j].stock;
        pricingVariant.push(variasi.value[i].sizes[j].price);
      }
    }

      const minVariasi = Math.min.apply(
      Math,
      pricingVariant
    );

    const maxVariasi = Math.max.apply(
      Math,
      pricingVariant
    );
    if (minVariasi == maxVariasi) {
      return numberWithCommas(minVariasi);
    }
    if (minVariasi != maxVariasi) {
      return `${numberWithCommas(minVariasi)} - ${numberWithCommas(maxVariasi)}`;
    }
  }

  if (sizes.filter((el) => el.price).length > 0) {
    const min = Math.min.apply(
      Math,
      sizes.filter((el) => el.price).map((el) => el.price)
    );
    const max = Math.max.apply(
      Math,
      sizes.filter((el) => el.price).map((el) => el.price)
    );
    if (min == max) {
      return numberWithCommas(min);
    }
    if (min != max) {
      return `${numberWithCommas(min)} - ${numberWithCommas(max)}`;
    }
  }else{
      if(discount){
          return numberWithCommas(price);
      }
      return numberWithCommas(price);
  }
}




      
