import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = {
    addressList: [{
        success: true,
        statusCode: 200,
        data: {}
    },]
}

export default {
    state,
    getters,
    actions,
    mutations
  };