import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import user from '../../../api/user'

export default {
  [actions.ADDRESS_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      user.getUser(payload.param).then(response => {
        context.commit(mutations.ADDRESS_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  
}
