import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = {
    orderList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    ulasanDetail: {
        success: true,
        statusCode: 200,

        order_details:[]
    }
    }


export default {
    state,
    getters,
    actions,
    mutations
  };