import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import ulsan from '../../../api/ulasan'

export default {
  [actions.ULASAN_DETAIL](context, payload) {
    return new Promise((resolve, reject) => {
      ulsan.getUlasanDetail(payload.param).then(response => {
        context.commit(mutations.ULASAN_DETAIL, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
}
