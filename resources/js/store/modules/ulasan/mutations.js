import * as types from '../../mutation-types'

export default {
  [ types.ULASAN_DETAIL] (state, data) {
    state.ulasanDetail = data
  },
}
