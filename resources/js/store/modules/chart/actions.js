import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import chart from '../../../api/chart'

export default {
  [actions.HISTORY_PRODUCTS_CHART](context, payload) {
    return new Promise((resolve, reject) => {
      chart.getHistoryProductsChart(payload.param).then(response => {
        context.commit(mutations.HISTORY_PRODUCTS_CHART, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
    
  }
}
