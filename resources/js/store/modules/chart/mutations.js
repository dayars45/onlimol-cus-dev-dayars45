import * as types from '../../mutation-types'

export default {
  [ types.HISTORY_PRODUCTS_CHART ] (state, data) {
    state.historyProductsChart = data
  },
}
