import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = {
  historyProductsChart: {
    success: true,
    statusCode: 200,
    count: 7,
    totalPages: 1,
    currentPage: 1,
    paging: {
        currentPage: 1,
        nextPage: 0,
        prevPage: 0,
        totalPage: 1,
        totalData: 2,
        limit: 24
    },
    data: [
        {
            variasi: {
                key1: "Variasi",
                "key2": "Pilihan",
                "value": [{
                  "name": "",
                  "image": "",
                  "_id": "",
                  "sizes": []
                 }]      
            },
            "ulasan":[],
            "discount_price": null,
            "discount": 35,
            "stock": 210,
            "pricing": [],
            "channel_id": null,
            "mall": false,
            "flashSale": 1,
            "description": "",
            "seller_id": {
                "default_location": {
                    "origin_province": 3,
                    "origin_province_name": "Banten",
                    "origin_city": 456,
                    "origin_city_name": "Tangerang",
                    "origin_address": "Grand Duta",
                    "origin_district": 6307,
                    "origin_district_name": "Periuk",
                    "origin_postal_code": null,
                    "origin_geo": null
                },
                "name": "default Seller",
                "_id": "60192860fec7855ba2baff1d",
                "mall_id": null
            },
            "weight": 0,
            "status": 1,
            "sold_out": 0,
            "_id": "5fe1e377360500687ada3389",
            "images": [],
            "category_id": "5fdf634344afad712f58358e",
            "item_id": "5fe4b712830fe9679d80cb02",
            "name": "...Loading",
            "code": "SKU03",
            "image": "web/images/default/product.png",
            "price": 0,
            "colors": [],
            "sizes": [],
            "__v": 0
        }
    ]
},
}

export default {
    state,
    getters,
    actions,
    mutations
  };