import * as types from '../../mutation-types'

export default {
  [ types.TRANSAKSI_LIST ] (state, data) {
    state.transaksiList = data
  },
}
