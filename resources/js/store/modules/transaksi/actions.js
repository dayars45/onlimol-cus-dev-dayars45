import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import transaksi from '../../../api/transaksi'

export default {
  [actions.TRANSAKSI_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      transaksi.getTransaksiList(payload.param).then(response => {
        context.commit(mutations.TRANSAKSI_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
}
