import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = {
   slider: {
    success: true,
    statusCode: 200,
    count: 24,
    totalPages: 2,
    currentPage: "1",
    data: []
  }
}

export default {
    state,
    getters,
    actions,
    mutations
  };