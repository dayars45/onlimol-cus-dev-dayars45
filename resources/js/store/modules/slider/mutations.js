import * as types from '../../mutation-types'

export default {
  [ types.SLIDER ] (state, data) {
    state.slider = data
  },
}
