import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import slider from '../../../api/slider'

export default {
  [actions.SLIDER](context, payload) {
    return new Promise((resolve, reject) => {
      slider.getSlider(payload.param).then(response => {
        context.commit(mutations.SLIDER, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
    
  }
}
