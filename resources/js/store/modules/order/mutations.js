import * as types from '../../mutation-types'

export default {
  [ types.ORDER_LIST ] (state, data) {
    state.orderList = data
  },
  [ types.CHART_LIST ] (state, data) {
    state.chartList = data
  },
}
