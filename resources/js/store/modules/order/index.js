import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = {
    orderList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    chartList:[
        {
            "user_id": "615e47c133aed82786257355",
            "doc_no": "20220103031100171",
            "invoice_number": "INV/20220103031100171",
            "seller_id": {
                "default_location": {
                    "origin_province": 3,
                    "origin_province_name": "Banten",
                    "origin_city": 456,
                    "origin_city_name": "Tangerang",
                    "origin_address": "Grand Duta",
                    "origin_district": 6307,
                    "origin_district_name": "Periuk",
                    "origin_postal_code": null,
                    "origin_geo": null
                },
                "name": "Yadi's",
                "mall_id": null,
                "photo": "https://res.cloudinary.com/tangerangstore/image/upload/v1638887084/cevetuqfxqjyjqmfy6et.png",
                "_id": "618cb04458172d6f79d90a42",
                "id": "618cb04458172d6f79d90a42"
            },
            "payment_status": "unpaid",
            "payment_method": "cash",
            "order_status": "unpaid",
            "origin_info": {
                "origin_province": 3,
                "origin_province_name": "Banten",
                "origin_city": 456,
                "origin_city_name": "Tangerang",
                "origin_address": "Grand Duta",
                "origin_district": 6307,
                "origin_district_name": "Periuk",
                "origin_postal_code": null,
                "origin_geo": null,
                "location_name": "Rumah",
                "receiver_phone": "085280378169",
                "sender_name": "Ibu Lilah ",
                "sender_phone": "085817564773",
                "receiver_name": "Ibu Lilah",
                "destination_address": "Perumahan Bumi Asri Jalan manga 6 blok F 6 No 8",
                "destination_province": 3,
                "destination_province_name": "Banten",
                "destination_city": 455,
                "destination_city_name": "Kabupaten Tangerang",
                "destination_district": 6287,
                "destination_district_name": "Pasar Kemis",
                "destination_postal_code": "15561",
                "destination_geo": null,
                "destination_loc": {
                    "lat": null,
                    "lon": null
                }
            },
            "order_details": [
                {
                    "category_id": "5fdf6a4444afad712f583599",
                    "category_name": "Sepatu Pria",
                    "item_id": "613a07964aa18c2e36da49ea",
                    "item_name": "Sneaker",
                    "color_id": "",
                    "color_name": "",
                    "size_id": 0,
                    "size_name": "38",
                    "product_id": "618f994f64cc84342238e7e1",
                    "product_name": "Sepatu Converse CT All Star Fashion Skull Bones Made In Vietnam BNIB",
                    "product_image": "https://firebasestorage.googleapis.com/v0/b/tangerang-web.appspot.com/o/21122210224053671b10ee0280807c2e71c06d491bc7.jfif?alt=media&token=102be31a-1a17-4190-9f13-a6f7db88d27a",
                    "product_price": 85000,
                    "subtotal_price": 85000,
                    "weight": 1000,
                    "total_weight": 1000,
                    "quantity": 1,
                    "tax": 0,
                    "discount": 32
                }
            ]
        },
        {
            "user_id": "615e47c133aed82786257355",
            "doc_no": "20220103031115171",
            "invoice_number": "INV/20220103031115171",
            "seller_id": {
                "default_location": {
                    "origin_province": 3,
                    "origin_province_name": "Banten",
                    "origin_city": 456,
                    "origin_city_name": "Tangerang",
                    "origin_address": "Grand Duta",
                    "origin_district": 6307,
                    "origin_district_name": "Periuk",
                    "origin_postal_code": null,
                    "origin_geo": null
                },
                "name": "Meandboss.co",
                "mall_id": null,
                "photo": "https://res.cloudinary.com/tangerangstore/image/upload/v1640040852/ta4bfe2olyexmsukimcs.jpg",
                "_id": "61a074299d15fbeaa21cc122",
                "id": "61a074299d15fbeaa21cc122"
            },
            "payment_status": "unpaid",
            "payment_method": "cash",
            "order_status": "unpaid",
            "origin_info": {
                "origin_province": 3,
                "origin_province_name": "Banten",
                "origin_city": 456,
                "origin_city_name": "Tangerang",
                "origin_address": "Grand Duta",
                "origin_district": 6307,
                "origin_district_name": "Periuk",
                "origin_postal_code": null,
                "origin_geo": null,
                "location_name": "Rumah",
                "receiver_phone": "085280378169",
                "sender_name": "Ibu Lilah ",
                "sender_phone": "085817564773",
                "receiver_name": "Ibu Lilah",
                "destination_address": "Perumahan Bumi Asri Jalan manga 6 blok F 6 No 8",
                "destination_province": 3,
                "destination_province_name": "Banten",
                "destination_city": 455,
                "destination_city_name": "Kabupaten Tangerang",
                "destination_district": 6287,
                "destination_district_name": "Pasar Kemis",
                "destination_postal_code": "15561",
                "destination_geo": null,
                "destination_loc": {
                    "lat": null,
                    "lon": null
                }
            },
            "order_details": [
                {
                    "category_id": "61c6c369cb5d9d19c7869cea",
                    "category_name": "Pakaian Anak",
                    "item_id": "61c6c95ecb5d9d19c7869ceb",
                    "item_name": "Sepatu Anak",
                    "color_id": 0,
                    "color_name": "21",
                    "size_id": 0,
                    "size_name": "Hitam",
                    "product_id": "61b9b8fbcd305e6ef9c60022",
                    "product_name": "Sepatu Sneaker Anak Laki Perempuan Lampu LED Import",
                    "product_image": "https://firebasestorage.googleapis.com/v0/b/tangerang-web.appspot.com/o/21121516425595856ac8cea55711abb46d4e01c3aa29.jfif?alt=media&token=b93acdbc-842f-408f-aa7e-b12cbf112d67",
                    "product_price": 97500,
                    "subtotal_price": 97500,
                    "weight": 300,
                    "total_weight": 300,
                    "quantity": 1,
                    "tax": 0,
                    "discount": 25
                }
            ]
        }
    ]
}

export default {
    state,
    getters,
    actions,
    mutations
  };