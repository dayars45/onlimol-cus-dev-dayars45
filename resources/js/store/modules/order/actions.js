import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import order from '../../../api/order'

export default {
  [actions.ORDER_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      order.getOrderList(payload.param).then(response => {
        context.commit(mutations.ORDER_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.CHART_LIST](context, payload) {
    context.commit(mutations.CHART_LIST, payload)
  },
}
