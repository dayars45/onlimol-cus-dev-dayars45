import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import category from '../../../api/category'

export default {
  [actions.CATEGORY](context, payload) {
    return new Promise((resolve, reject) => {
      category.getCategoryList(payload.param).then(response => {
        context.commit(mutations.CATEGORY, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.CATEGORY_HEADER](context, payload) {
    return new Promise((resolve, reject) => {
      category.getCategoryList(payload.param).then(response => {
        context.commit(mutations.CATEGORY_HEADER, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.CATEGORY_PREFERRED](context, payload) {
    return new Promise((resolve, reject) => {
      category.getCategoryList(payload.param).then(response => {
        context.commit(mutations.CATEGORY_PREFERRED, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.CATEGORY_MENU](context, payload) {
    return new Promise((resolve, reject) => {
      category.getCategoryList(payload.param).then(response => {
        context.commit(mutations.CATEGORY_MENU, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.CATEGORY_SILDER](context, payload) {
    return new Promise((resolve, reject) => {
      category.getCategoryList(payload.param).then(response => {
        context.commit(mutations.CATEGORY_SILDER, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  
  
}
