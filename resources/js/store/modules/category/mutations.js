import * as types from '../../mutation-types'

export default {
  [ types.CATEGORY ] (state, data) {
    state.searchingCategoryList = data
  },
  [ types.CATEGORY_HEADER ] (state, data) {
    state.categoryHeader = data
  },
  [ types.CATEGORY_PREFERRED ] (state, data) {
    state.categoryFrefered = data
  },
  [ types.CATEGORY_MENU ] (state, data) {
    state.categoryMenu = data
  },
  [ types.CATEGORY_SILDER ] (state, data) {
    state.categorySlider = data
  },
}
