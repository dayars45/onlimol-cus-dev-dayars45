export default {
    searchingCategoryList: state => state.searchingCategoryList,
    categoryHeader: state => state.categoryHeader,
    categoryFrefered: state => state.categoryFrefered,
    categoryMenu: state => state.categoryMenu,
    categorySlider: state => state.categorySlider,
  }
