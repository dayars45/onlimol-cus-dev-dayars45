export default {
    rekomendasiList: state => state.rekomendasiList,
    bestSellerList: state => state.bestSellerList,
    optionProductList: state => state.optionProductList,
    moreProductsList: state => state.moreProductsList,
    trendingProductList: state => state.trendingProductList,
    anotherProductList: state => state.anotherProductList,
    relatedProductList: state => state.relatedProductList,
    reviewProductList: state => state.reviewProductList,
    discussProductList: state => state.discussProductList,
    bestReviewProduct: state => state.bestReviewProduct,
    getOneProduct: state => state.getOneProduct,
  }
