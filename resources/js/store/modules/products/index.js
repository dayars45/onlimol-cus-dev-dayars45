import getters from "./getters";
import actions from "./actions";
import mutations from "./mutations";

const state = {
    rekomendasiList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    moreProductsList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    bestSellerList: {
        success: true,
        statusCode: 200,
        count: 7,
        totalPages: 1,
        currentPage: 1,
        paging: {
            currentPage: 1,
            nextPage: 0,
            prevPage: 0,
            totalPage: 1,
            totalData: 2,
            limit: 24
        },
        data: [
            {
                variasi: {
                    key1: "Variasi",
                    "key2": "Pilihan",
                    "value": [{
                      "name": "",
                      "image": "",
                      "_id": "",
                      "sizes": []
                     }]      
                },
                "ulasan":[],
                "discount_price": null,
                "discount": 35,
                "stock": 210,
                "pricing": [],
                "channel_id": null,
                "mall": false,
                "flashSale": 1,
                "description": "",
                "seller_id": {
                    "default_location": {
                        "origin_province": 3,
                        "origin_province_name": "Banten",
                        "origin_city": 456,
                        "origin_city_name": "Tangerang",
                        "origin_address": "Grand Duta",
                        "origin_district": 6307,
                        "origin_district_name": "Periuk",
                        "origin_postal_code": null,
                        "origin_geo": null
                    },
                    "name": "default Seller",
                    "_id": "60192860fec7855ba2baff1d",
                    "mall_id": null
                },
                "weight": 0,
                "status": 1,
                "sold_out": 0,
                "_id": "5fe1e377360500687ada3389",
                "images": [],
                "category_id": "5fdf634344afad712f58358e",
                "item_id": "5fe4b712830fe9679d80cb02",
                "name": "...Loading",
                "code": "SKU03",
                "image": "web/images/default/product.png",
                "price": 0,
                "colors": [],
                "sizes": [],
                "__v": 0
            }
        ]
    },
    optionProductList: {
        success: true,
        statusCode: 200,
        count: 7,
        totalPages: 1,
        currentPage: 1,
        paging: {
            currentPage: 1,
            nextPage: 0,
            prevPage: 0,
            totalPage: 1,
            totalData: 2,
            limit: 24
        },
        data: [
            {
                variasi: {
                    key1: "Variasi",
                    "key2": "Pilihan",
                    "value": [{
                      "name": "",
                      "image": "",
                      "_id": "",
                      "sizes": []
                     }]      
                },
                "ulasan":[],
                "discount_price": null,
                "discount": 35,
                "stock": 210,
                "pricing": [],
                "channel_id": null,
                "mall": false,
                "flashSale": 1,
                "description": "",
                "seller_id": {
                    "default_location": {
                        "origin_province": 3,
                        "origin_province_name": "Banten",
                        "origin_city": 456,
                        "origin_city_name": "Tangerang",
                        "origin_address": "Grand Duta",
                        "origin_district": 6307,
                        "origin_district_name": "Periuk",
                        "origin_postal_code": null,
                        "origin_geo": null
                    },
                    "name": "default Seller",
                    "_id": "60192860fec7855ba2baff1d",
                    "mall_id": null
                },
                "weight": 0,
                "status": 1,
                "sold_out": 0,
                "_id": "5fe1e377360500687ada3389",
                "images": [],
                "category_id": "5fdf634344afad712f58358e",
                "item_id": "5fe4b712830fe9679d80cb02",
                "name": "...Loading",
                "code": "SKU03",
                "image": "web/images/default/product.png",
                "price": 0,
                "colors": [],
                "sizes": [],
                "__v": 0
            }
        ]
    },
    moreProductList: {
        success: true,
        statusCode: 200,
        count: 7,
        totalPages: 1,
        currentPage: 1,
        paging: {
            currentPage: 1,
            nextPage: 0,
            prevPage: 0,
            totalPage: 1,
            totalData: 2,
            limit: 24
        },
        data: [
            {
                variasi: {
                    key1: "Variasi",
                    "key2": "Pilihan",
                    "value": [{
                      "name": "",
                      "image": "",
                      "_id": "",
                      "sizes": []
                     }]      
                },
                "ulasan":[],
                "discount_price": null,
                "discount": 35,
                "stock": 210,
                "pricing": [],
                "channel_id": null,
                "mall": false,
                "flashSale": 1,
                "description": "",
                "seller_id": {
                    "default_location": {
                        "origin_province": 3,
                        "origin_province_name": "Banten",
                        "origin_city": 456,
                        "origin_city_name": "Tangerang",
                        "origin_address": "Grand Duta",
                        "origin_district": 6307,
                        "origin_district_name": "Periuk",
                        "origin_postal_code": null,
                        "origin_geo": null
                    },
                    "name": "default Seller",
                    "_id": "60192860fec7855ba2baff1d",
                    "mall_id": null
                },
                "weight": 0,
                "status": 1,
                "sold_out": 0,
                "_id": "5fe1e377360500687ada3389",
                "images": [],
                "category_id": "5fdf634344afad712f58358e",
                "item_id": "5fe4b712830fe9679d80cb02",
                "name": "...Loading",
                "code": "SKU03",
                "image": "web/images/default/product.png",
                "price": 0,
                "colors": [],
                "sizes": [],
                "__v": 0
            }
        ]
    },
    trendingProductList: {
        success: true,
        statusCode: 200,
        count: 7,
        totalPages: 1,
        currentPage: 1,
        paging: {
            currentPage: 1,
            nextPage: 0,
            prevPage: 0,
            totalPage: 1,
            totalData: 2,
            limit: 24
        },
        data: [
            {
                variasi: {
                    key1: "Variasi",
                    "key2": "Pilihan",
                    "value": [{
                      "name": "",
                      "image": "",
                      "_id": "",
                      "sizes": []
                     }]      
                },
                "ulasan":[],
                "discount_price": null,
                "discount": 35,
                "stock": 210,
                "pricing": [],
                "channel_id": null,
                "mall": false,
                "flashSale": 1,
                "description": "",
                "seller_id": {
                    "default_location": {
                        "origin_province": 3,
                        "origin_province_name": "Banten",
                        "origin_city": 456,
                        "origin_city_name": "Tangerang",
                        "origin_address": "Grand Duta",
                        "origin_district": 6307,
                        "origin_district_name": "Periuk",
                        "origin_postal_code": null,
                        "origin_geo": null
                    },
                    "name": "default Seller",
                    "_id": "60192860fec7855ba2baff1d",
                    "mall_id": null
                },
                "weight": 0,
                "status": 1,
                "sold_out": 0,
                "_id": "5fe1e377360500687ada3389",
                "images": [],
                "category_id": "5fdf634344afad712f58358e",
                "item_id": "5fe4b712830fe9679d80cb02",
                "name": "...Loading",
                "code": "SKU03",
                "image": "web/images/default/product.png",
                "price": 0,
                "colors": [],
                "sizes": [],
                "__v": 0
            }
        ]
    },
    anotherProductList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    relatedProductList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    reviewProductList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    discussProductList: {
        success: true,
        statusCode: 200,
        count: 24,
        totalPages: 2,
        currentPage: "1",
        data: []
    },
    getOneProduct: {
        success: true,
        statusCode: 200,
        data: {
            variasi: {
                key1:"",
                key2:"",
                value: []
            },
            item_id: {
                _id: "",
                name: "", 
            },
            brand_id: "",
            discount_price: "",
            discount: "",
            stock: "",
            pricing: [],
            is_danger: false,
            sort: 0,
            channel_id: "",
            mall: false,
            flashSale: "",
            type: "",
            jasa_type: "",
            description: "",
            seller_id: "",
            weight: 0,
            width: 0,
            long: 0,
            height: 0,
            combo: false,
            status: 0,
            sold_out: 0,
            pre_order: false,
            condition: "",
            sku: "",
            kurir: [],
            category_id: {
                _id: "",
                name: "",
                id: ""
            },
            name: "",
            price: 0,
            sizes: [],
            grosir: [],
            image: "",
            images: [],
            colors: [],
        }
    },
}

export default {
    state,
    getters,
    actions,
    mutations
  };