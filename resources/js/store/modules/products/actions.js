import * as mutations from '../../mutation-types'
import * as actions from '../../action-types'
import product from '../../../api/product'
import order from '../../../api/order'

export default {
  [actions.REKOMENDASI_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getProdukList(payload.param).then(response => {
        context.commit(mutations.REKOMENDASI_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.BEST_SELLER_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getProdukBaseSallerList(payload.param).then(response => {
        context.commit(mutations.BEST_SELLER_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.OPTION_PRODUCT](context, payload) {
    return new Promise((resolve, reject) => {
      product.getOptionProductList(payload.param).then(response => {
        context.commit(mutations.OPTION_PRODUCT, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.MORE_PRODUCTS_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getMoreProdukList(payload.param).then(response => {
        context.commit(mutations.MORE_PRODUCTS_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.TRENDING_PRODUCT](context, payload) {
    return new Promise((resolve, reject) => {
      product.getTrendingProductList(payload.param).then(response => {
        context.commit(mutations.TRENDING_PRODUCT, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.ANOTHER_PRODUCT_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getProdukList(payload.param).then(response => {
        context.commit(mutations.ANOTHER_PRODUCT_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.RELATED_PRODUCT_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getProdukList(payload.param).then(response => {
        context.commit(mutations.RELATED_PRODUCT_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.REVIEW_PRODUCT_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getReviewProdukList(payload.param).then(response => {
        context.commit(mutations.REVIEW_PRODUCT_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.DISCUSS_PRODUCT_LIST](context, payload) {
    return new Promise((resolve, reject) => {
      product.getDiscussProdukList(payload.param).then(response => {
        context.commit(mutations.DISCUSS_PRODUCT_LIST, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.BEST_REVIEW_PRODUCT](context, payload) {
    return new Promise((resolve, reject) => {
      product.getReviewProdukList(payload.param).then(response => {
        context.commit(mutations.BEST_REVIEW_PRODUCT, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  [actions.GET_ONE_PRODUCT](context, payload) {
    return new Promise((resolve, reject) => {
      product.getOneProduct(payload.param).then(response => {
        context.commit(mutations.GET_ONE_PRODUCT, response.data)
        resolve(response)
      }).catch(error => {
        reject(error)
      })
    })
  },
  
}
