import * as types from '../../mutation-types'

export default {
  [ types.REKOMENDASI_LIST ] (state, data) {
    state.rekomendasiList = data
  },
  [ types.BEST_SELLER_LIST ] (state, data) {
    state.bestSellerList = data
  },
  [ types.OPTION_PRODUCT ] (state, data) {
    state.optionProductList = data
  },
  [ types.MORE_PRODUCTS_LIST ] (state, data) {
    state.moreProductsList = data
  },
  [ types.TRENDING_PRODUCT ] (state, data) {
    state.trendingProductList = data
  },
  [ types.ANOTHER_PRODUCT_LIST ] (state, data) {
    state.anotherProductList = data
  },
  [ types.RELATED_PRODUCT_LIST ] (state, data) {
    state.relatedProductList = data
  },
  [ types.REVIEW_PRODUCT_LIST ] (state, data) {
    state.reviewProductList = data
  },
  [ types.DISCUSS_PRODUCT_LIST ] (state, data) {
    state.discussProductList = data
  },
  [ types.BEST_REVIEW_PRODUCT ] (state, data) {
    state.bestReviewProduct = data
  },
  [ types.GET_ONE_PRODUCT ] (state, data) {
    state.getOneProduct = data
  },
  
}
