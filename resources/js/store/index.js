import Vue from 'vue';
import Vuex from 'vuex';
import product from './modules/products';
import category from './modules/category';
import slider from './modules/slider';
import historyProductsChart from './modules/chart';
import order from './modules/order';
import ulasan from './modules/ulasan';
import user from './modules/user';
import transaksi from './modules/transaksi';
import createPersistedState from "vuex-persistedstate";


Vue.use(Vuex)
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    product,
    category,
    slider,
    historyProductsChart,
    order,
    ulasan,
    user,
    transaksi,
  },
  plugins: [createPersistedState()],
  strict: debug
})
