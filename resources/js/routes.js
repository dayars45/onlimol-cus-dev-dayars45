import Index from './components/pages/Index';
import Login from './components/pages/Login';
import Chat from './components/pages/Chat';
import Chart from './components/pages/Chart';
import Checkout from './components/pages/Checkout';
import ListTransaction from './components/pages/List-Transaction';
import ProductDetail from './components/pages/Product-Detail';
import ReviewDetail from './components/pages/Review-Detail';
import Discussion from './components/pages/Discussion';
import WaitingPayment from './components/pages/Waiting-Payment';
import Notification from './components/pages/Notification';
import OrderServiceInfo from './components/pages/Order-Service';
import ComplainedOrder from './components/pages/Complained-Order';
import Profile from './components/pages/Profile';
import Ulasan from './components/pages/Ulasan';
import UlasanOld from './components/pages/Ulasan-Old';
import Register from './components/pages/Register';

export const routes = [
    {
        name: '',
        path: '/',
        component: Index
    },
    {
        name: 'login',
        path: '/login',
        component: Login
    },
    {
        name: 'chat',
        path: '/chat',
        component: Chat
    },
    {
        name: 'chart',
        path: '/chart',
        component: Chart
    },
    {
        name: 'checkout',
        path: '/checkout',
        component: Checkout
    },
    {
        name: 'list-transaction',
        path: '/list-transaction',
        component: ListTransaction
    },  
    {
        name: 'review-detail',
        path: '/review-detail',
        component: ReviewDetail
    },
    {
        name: 'discussion',
        path: '/discussion',
        component: Discussion
    },
    {
        name: 'waiting-payment',
        path: '/waiting-payment',
        component: WaitingPayment
    },
    {
        name: 'notification',
        path: '/notification',
        component: Notification
    },
    {
        name: 'complained-order',
        path: '/complained-order',
        component: ComplainedOrder
    },
    {
        name: 'profile',
        path: '/profile',
        component: Profile
    },
    {
        name: 'ulasan',
        path: '/ulasan',
        component: Ulasan
    },
    {
        name: 'ulasan-old',
        path: '/ulasan-old',
        component: UlasanOld
    },
    {

        name: 'order-Service',
        path: '/order-service',
        component: OrderServiceInfo
    },
    {
        name: 'register',
        path: '/register',
        component: Register
    },
    {
        path: '/:name',
        name: 'product',
        component: ProductDetail
     },
]