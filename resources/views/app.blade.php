<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}" />
    <title>Development</title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/mdb.min.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/owl.carousel/dist/assets/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/slick/slick/slick.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/slick/slick/slick-theme.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/@fortawesome/fontawesome-free/css/all.min.css')}}">
    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
</head>

<body>
    <div id="app"></div>
    <script src="{{ asset('assets/js/jquery/dist/jquery.js')}}"></script>
    <script src="{{ asset('assets/js/mdb.min.js')}}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('assets/plugins/owl.carousel/dist/owl.carousel.js')}}"></script>
    <script src="{{ asset('assets/plugins/slick/slick/slick.js')}}"></script>
    <script src="{{ asset('assets/js/custom.js')}}"></script>
    <script src="{{ asset('assets/js/navbar.js')}}"></script>
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>

</html>